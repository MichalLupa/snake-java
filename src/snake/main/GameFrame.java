package snake.main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;


public class GameFrame extends JFrame implements ActionListener {
    final MenuPanel menuPanel;
    final BoardPanel boardPanel;
    final GameOverPanel gameOverPanel;
    final RankingPanel rankingPanel;
    Board board;
    GameLoop gameLoop;
    Ranking highScoreRecords = new Ranking("Ranking.txt");

 
    GameFrame(Dimension size) {
        setSize(size);
        setMinimumSize(size);

        setTitle("Snake");

        menuPanel = new MenuPanel(this);
        boardPanel = new BoardPanel(true);
        gameOverPanel = new GameOverPanel(this);
        rankingPanel = new RankingPanel(this);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        JPanel[] panels = {menuPanel, boardPanel, gameOverPanel, rankingPanel};
        for (JPanel panel: panels) {
            panel.setVisible(true);
        }
        setContentPane(menuPanel);
        highScoreRecords.readFromFile();
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        JPanel panelToDisplay = null;
        switch (e.getActionCommand()){
            case "START":
                try {
                    int fruitCount = 3;
                    int frogCount = 1;
                    BoardBuilder boardBuilder = new BoardBuilder(40, 40, fruitCount, frogCount,6, 5, 20, 3);
                    board = boardBuilder.generateBoard();
                    boardPanel.setCurrentBoard(board);
                    gameLoop = new GameLoop(board, boardPanel, this);
                    setContentPane(boardPanel);
                    gameLoop.start();
                } catch (TileOutOfBoundsException exception) {
                    System.out.println("Invalid board generated");
                    return;
                }
                panelToDisplay = boardPanel;
                break;
            case "RANKING":
                panelToDisplay = rankingPanel;
                highScoreRecords.readFromFile();
                rankingPanel.setHighScoreRecords(highScoreRecords);
                break;
            case "MENU":
                panelToDisplay = menuPanel;
                break;
            case "GAME_OVER":
                panelToDisplay = gameOverPanel;
                gameOverPanel.reset();
                gameOverPanel.setHighScoreRecords(highScoreRecords);
                gameOverPanel.setScore(gameLoop.getLastScore());
                break;
            default:
        }
        if (panelToDisplay != null)
        {
            setContentPane(panelToDisplay);
            revalidate();
            repaint();
        }
    }
}
