package snake.main;

import java.util.ArrayList;
import java.util.Random;

public class BoardBuilder {
    private final boolean[][] occupiedTiles;
    private final int boardWidth;
    private final int boardHeight;
    private final int fruitCount;
    private final int frogCount;
    private final int obstaclesCount;
    private final int snakeLength;
    private final int minObstacleLength;
    private final int maxObstacleLength;
    private final Random rand;

    BoardBuilder(int boardWidth, int boardHeight, int fruitCount, int frogCount, int obstaclesCount,
                   int minObstacleLength, int maxObstacleLength, int snakeLength) {
        this.rand = new Random();
        this.occupiedTiles = new boolean[boardWidth][boardHeight];
        this.boardWidth = boardWidth;
        this.boardHeight = boardHeight;
        this.fruitCount = fruitCount;
        this.frogCount = frogCount;
        this.obstaclesCount = obstaclesCount;
        this.minObstacleLength = minObstacleLength;
        this.maxObstacleLength = maxObstacleLength;
        this.snakeLength = snakeLength;
    }

    /***
     * Ustawia podane wspolrzedne na uzywane 
     * @param cords wspolrzedne ktore maja zostac oznaczone
     */
    private void markCoordinatesAsOccupied(Cords cords) {
        this.occupiedTiles[cords.x][cords.y] = true;
    }

    /***
     * Ustawia wspolrzedne na nieuzywane
     */
    private Cords generateUnoccupiedCoordinates(int minX, int maxX, int minY, int maxY) {
        int xCoordinate = minX + this.rand.nextInt(maxX - minX);
        int yCoordinate = minY + this.rand.nextInt(maxY - minY);

        while (this.occupiedTiles[xCoordinate][yCoordinate]) {
            xCoordinate = minX + this.rand.nextInt(maxX - minX);
            yCoordinate = minY + this.rand.nextInt(maxY - minY);
        }
        return new Cords(xCoordinate, yCoordinate);
    }

    /***
     * Ustawia losowo nieuzywane wspolrzedne
     */
    private Cords generateUnoccupiedCoordinates() {
        return generateUnoccupiedCoordinates(0, this.boardWidth, 0, this.boardHeight);
    }


    private Cords generateUnoccupiedCoordinatesAndMarkAsOccupied(int minX, int maxX, int minY, int maxY) {
        Cords cords = generateUnoccupiedCoordinates(minX, maxX, minY, maxY);
        markCoordinatesAsOccupied(cords);
        return cords;
    }


    private Cords generateUnoccupiedCoordinatesAndMarkAsOccupied() {
        return generateUnoccupiedCoordinatesAndMarkAsOccupied(0, this.boardWidth, 0, this.boardHeight);
    }

    /***
     * Ustawia nowego weza na nieuzywanych wspolrzednych
     * @return wygenerowany waz
     */
    private Snake generateSnake() {
        Direction snakeDirection = Direction_Utils.getRandomDirection();
        ArrayList<Cords> snakeSegment = generateLineSegment(this.snakeLength, snakeDirection);
        Cords snakeHead = snakeSegment.get(0);
        for (Cords segmentCoordinate : snakeSegment) {
            this.occupiedTiles[segmentCoordinate.x][segmentCoordinate.y] = true;
        }
        snakeSegment.remove(0);
        return new Snake(snakeSegment, snakeHead, Direction_Utils.getOppositeDirection(snakeDirection));
    }

    /***
     * Ustawia na wszystkich nieuzywanych wspolrzednych nowe obiekty
     */
    public Board generateBoard() throws TileOutOfBoundsException {
        Board generatedBoard = new Board(this.boardWidth, this.boardHeight);

        for (int i = 0; i < this.fruitCount; i++) {
            Fruit fruit = new Fruit(generateUnoccupiedCoordinatesAndMarkAsOccupied());
            generatedBoard.addFruit(fruit);
        }
        for (int i = 0; i < this.frogCount; i++) {
            Frog frog = new Frog(generateUnoccupiedCoordinatesAndMarkAsOccupied());
            generatedBoard.addFrog(frog);
        }
        for (int i = 0; i < this.obstaclesCount; i++) {
            int segmentLength = this.minObstacleLength + rand.nextInt(this.maxObstacleLength - this.minObstacleLength);
            ArrayList<Cords> obstacleSegment = generateLineSegment(segmentLength, Direction_Utils.getRandomDirection());
            for (Cords segmentCoordinate : obstacleSegment) {
                generatedBoard.addObstacle(segmentCoordinate);
                this.occupiedTiles[segmentCoordinate.x][segmentCoordinate.y] = true;
            }
        }
        generatedBoard.setPlayerSnake(generateSnake());
        generatedBoard.setEnemySnake(generateSnake());
        return generatedBoard;
    }

    /***
     * Sprawdza czy wspolrzedne nie sa poza zakresem planszy
     * @param cords wspolrzedne do sprawdzenia
     * @param margin zakres planszy
     * @return Zwraca True jesli wspolrzedne sa poza zakresem i False jesli nie sa
     */
    private boolean areCoordinatesNotInBounds(Cords cords, int margin) {
        return cords.x < margin || cords.x >= this.boardWidth - margin ||
                cords.y < margin || cords.y >= this.boardHeight - margin;
    }


    private boolean areAllCoordinatesFree(Cords startingPoint, Cords endPoint, int margin) {
        if (areCoordinatesNotInBounds(startingPoint, margin) || areCoordinatesNotInBounds(endPoint, margin)) {
            return false;
        }
        for (int i = Math.min(startingPoint.x, endPoint.x) - margin; i <= Math.max(startingPoint.x, endPoint.x) + margin; i++) {
            for (int j = Math.min(startingPoint.y, endPoint.y) - margin; j <= Math.max(startingPoint.y, endPoint.y) + margin; j++) {
                if (this.occupiedTiles[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }


    private ArrayList<Cords> generateLineSegment(int segmentLength, Direction direction) {
        ArrayList<Cords> outputCoordinates = new ArrayList<>();
        int[] deltas = Direction_Utils.getDeltas(direction);
        int deltaX = deltas[0];
        int deltaY = deltas[1];
        boolean isRoomForSegment = false;

        while (!isRoomForSegment) {
            Cords startingPoint = generateUnoccupiedCoordinates();
            Cords endPoint = new Cords(startingPoint.x + deltaX * (segmentLength - 1),
                                                   startingPoint.y + deltaY * (segmentLength - 1));

            isRoomForSegment = areAllCoordinatesFree(startingPoint, endPoint, 3);

            if (isRoomForSegment) {
                for (int i = 0; i < segmentLength; i++) {
                    Cords cords = new Cords(startingPoint.x + deltaX * i, startingPoint.y + deltaY * i);
                    outputCoordinates.add(cords);
                }
            }
        }
        return outputCoordinates;
    }
}
