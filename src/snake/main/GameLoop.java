package snake.main;

import java.awt.event.ActionEvent;
import java.sql.Timestamp;
import java.util.*;

/***
 * Klasa do przebiegu gry, z kazda nowa uruchomiona gra jest tworzona nowa
 */
public class GameLoop extends Thread{
    final Board board;
    final BoardPanel boardPanel;
    boolean notPaused;
    boolean gameOver;
    int currentScore;
    KeyboardControler keyboardControler;
    GameFrame gameFrame;
    SnakeAI snakeAI;
    FrogAI frogAI;

    Snake snake;
    Snake enemySnake;
    ArrayList<Frog> frogs;
    ArrayList<Fruit> fruits;
    ArrayList<ObjectThread> objectThreads = new ArrayList<>();

    /***
     * GameLoop konstruktor
     * @param board plansza do gry
     * @param boardPanel wyswietla zmiany na planszy
     * @param gameFrame okno pozwalajace wyswietlic gre
     */
    GameLoop(Board board, BoardPanel boardPanel, GameFrame gameFrame){
        this.board = board;
        this.boardPanel = boardPanel;
        this.gameFrame = gameFrame;

        this.gameOver = false;
        this.notPaused = true;

        this.keyboardControler = new KeyboardControler();
        this.gameFrame.addKeyListener(keyboardControler);

        this.gameFrame.setFocusable(true);


        this.snake = board.getPlayerSnake();
        this.enemySnake = board.getEnemySnake();
        this.frogs = board.getFrogs();
        this.fruits = board.getFruits();

        this.snakeAI = new SnakeAI(board, this.enemySnake);
        this.frogAI = new FrogAI(board, frogs.get(0));

        this.currentScore = 0;
    }

    /***
     * Pozwala pobrac wynik gracza
     * @return zwraca wynik
     */
    public int getLastScore() {
        return currentScore;
    }

    private void updateState() {
        for (ObjectThread objectThread : this.objectThreads) {
            if (objectThread.getRelatedGameObject() != null) {
                objectThread.performNextAction();
            }
        }
        if (snake != null) {
            currentScore = snake.getSnakeBody().size() + 1 - 3;
        }
        if (board.getPlayerSnake() == null) {
            gameOver = true;
        }
    }

    private void render() {
        this.boardPanel.setCurrentBoard(board);
        this.gameFrame.revalidate();
        this.gameFrame.repaint();
    }

    private void removeGameObjects() {
        Set<Collisions> gameObjectsToRemove = new HashSet<>();
        for (ObjectThread objectThread : this.objectThreads) {
            gameObjectsToRemove.addAll(objectThread.getGameObjectsToRemove());
        }
        gameObjectsToRemove.removeIf(Objects::isNull);
        if (!gameObjectsToRemove.isEmpty()) {
            for (Collisions gameObject: gameObjectsToRemove) {
                objectThreads.removeIf( gameObjectThread -> gameObjectThread.getRelatedGameObject() == gameObject );
                board.removeGameObject(gameObject);
            }
        }
        objectThreads.removeIf( gameObjectThread -> gameObjectThread.getRelatedGameObject() == null );
    }

    private void cleanUpAndExit() {
        for (ObjectThread objectThread: objectThreads) {
            objectThread.forceKill();
        }
        objectThreads.clear();
        gameFrame.actionPerformed(new ActionEvent(gameFrame, ActionEvent.ACTION_PERFORMED, "GAME_OVER"));
    }


    public void run() {
        boardPanel.setCurrentBoard(board);
        this.objectThreads.add(new RivalSnake(this.board));
        this.objectThreads.add(new PlayerSnakeThread(this.board, this.keyboardControler));
        this.objectThreads.add(new Fruits_FrogsGeneratorTh(this.board));
        for (Frog frog : this.board.getFrogs()) {
            this.objectThreads.add(new FrogTh(this.board, frog));
        }
        for (ObjectThread objectThread : this.objectThreads) {
            objectThread.start();
        }
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long startTime = timestamp.getTime();
        long stopTime;
        while (!gameOver) {
            for (Frog frog : this.board.getMissingFrogs()) {
                ObjectThread objectThread = new FrogTh(this.board, frog);
                this.objectThreads.add(objectThread);
                objectThread.start();
            }
            this.board.clearMissingFrogs();

            for (ObjectThread objectThread : this.objectThreads) {
                objectThread.startCalculatingNextAction();
            }

            stopTime = timestamp.getTime();
            long timeDifference = startTime - stopTime;
            try {
                Thread.sleep(Math.max(0, 100 - timeDifference));
            } catch (Exception e) { }
            startTime = timestamp.getTime();

            updateState();
            removeGameObjects();
            render();
        }
        cleanUpAndExit();
    }
}
