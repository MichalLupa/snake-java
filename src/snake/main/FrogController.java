package snake.main;

import java.util.ArrayList;

/***
 * Uzywany do ruchu zab poprzez ustawianie ich wspolrzednych
 */
public class FrogController {
    /***
     * Pojedynczy ruch zaby
     * @param frog zaba ktora sie porusza
     */
    public static void move(Frog frog) {
        Cords newCoordinates = new Cords(frog.getCoordinates().x, frog.getCoordinates().y);
        newCoordinates.x += Direction_Utils.getDeltas(frog.getMoveDirection())[0];
        newCoordinates.y += Direction_Utils.getDeltas(frog.getMoveDirection())[1];
        frog.setCoordinates(newCoordinates);
    }

    /***
     * Sprawdzenie czy nastapila kolizja zaby z przeszkoda lub owocem
     * @return zwraca usuwany obiekt
     */
    public static ArrayList<Collisions> handleCollisions(Frog frog, Board board) {
        ArrayList<Collisions> gameObjectsToRemove = new ArrayList<>();
        Cords frogCoordinates = frog.getCoordinates();
        if (frogCoordinates.x < 0 || frogCoordinates.x >= board.getWidth() ||
                frogCoordinates.y < 0 || frogCoordinates.y >= board.getHeight()) {
            gameObjectsToRemove.add(frog);
        } else {
            Board.GameObjectArrayList[][] references = board.getReferenceMatrix(frog);
            ArrayList<Collisions> gameObjects =
                    references[frogCoordinates.x][frogCoordinates.y].gameObjects;
            if (!gameObjects.isEmpty()) {
                for (Collisions gameObject : gameObjects) {
                    switch (gameObject.getName()) {
                        case "Coordinates":
                        case "Snake":
                        case "Frog":
                            gameObjectsToRemove.add(frog);
                            break;
                        case "Fruit":
                            gameObjectsToRemove.add(gameObject);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        return gameObjectsToRemove;
    }

}
