package snake.main;

public interface AI {
    /***
     * zwraca nastepny ruch AI
     */
    public Direction getNextMoveDirection();
}
