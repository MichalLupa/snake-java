package snake.main;

/***
 * Klasa reprezentujaca zabe zawiera jej wspolrzedne
 */
public class Frog implements Collisions{
    private Cords cords;
    private Direction moveDirection;

    /***
     * konstruktor zaby
     * @param wspolrzedne zaby
     */
    Frog(Cords cords) {
        this.cords = new Cords(cords.x, cords.y);
    }

    /***
     * Zwraca aktualne polozenie zaby
     */
    public Cords getCoordinates() {
        return this.cords;
    }

    /***
     * Ustawia aktualne polozenie zaby
     */
    public void setCoordinates(Cords cords) {
        this.cords.x = cords.x;
        this.cords.y = cords.y;
    }

    /***
     * Pobiera kolejny ruch zaby i go zwraca
     */
    public Direction getMoveDirection() {
        return moveDirection;
    }

    /***
     * Ustawia kierunek ruchu zaby
     */
    public void setMoveDirection(Direction moveDirection) {
        this.moveDirection = moveDirection;
    }


    public String getName() {
        return "Frog";
    }
}
