package snake.main;

/***
 * Klasa reprezentujaca wspolrzedne
 */
public class Cords implements Collisions{
    public int x;
    public int y;

    /***
     * Konstruktor
     */
    Cords(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /***
     * Wyswietla wspolrzedne
     */
    public void print() {
        System.out.println("(" + this.x + ", " + this.y + ")");
    }

    public String getName() {
        return "Coordinates";
    }
}
