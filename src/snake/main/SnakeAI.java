package snake.main;

import java.util.ArrayList;

public class SnakeAI implements AI {
    private final Board board;
    private final Snake snake;

    SnakeAI(Board board, Snake snake) {
        this.board = board;
        this.snake = snake;
    }

    /***
     * Ruch wezem poprzez algorytm BF albo wygenerowany losowo
     * @return ruch w wybranym kierunku
     */
    public Direction getNextMoveDirection() {
        BFS bfs = new BFS(board);
        Direction nextDirection;
        try {
            ArrayList<Cords> path = bfs.getShortestPathToFrogOrFruit(snake.getSnakeHead());
            Cords nextMove = path.get(1);
            Cords currentPosition = snake.getSnakeHead();
            int[] deltas = {nextMove.x - currentPosition.x, nextMove.y - currentPosition.y};
            nextDirection = Direction_Utils.getDirection(deltas);
        } catch (Exception e) {
            nextDirection = Direction_Utils.getRandomDirection();
        }
        return nextDirection;
    }
}
