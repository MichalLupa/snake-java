package snake.main;

import java.util.ArrayList;

public class Fruits_FrogsGeneratorTh extends Thread implements ObjectThread {
    private Board board;
    private Fruits_Frogs_Generator fruits_Frogs_Generator;
    private boolean canCalculateNextAction = false;
    private ArrayList<Fruit> missingFruits;
    private ArrayList<Frog> missingFrogs;
    private boolean killed = false;

    Fruits_FrogsGeneratorTh(Board board) {
        this.board = board;
        this.fruits_Frogs_Generator = new Fruits_Frogs_Generator(board, 3, 1);
    }

    /***
     * Generuje kolejne jablka i zaby 
     */
    @Override
    public void run() {
        while(true) {
            if (killed) {
                break;
            }
            if (this.canCalculateNextAction) {
                this.missingFruits = fruits_Frogs_Generator.generateMissingFruitsList();
                this.missingFrogs = fruits_Frogs_Generator.generateMissingFrogsList();
                this.canCalculateNextAction = false;
            }
            try {
                Thread.sleep(1);
            } catch (Exception e) {
            }
        }
    }


    @Override
    public void startCalculatingNextAction() {
        this.canCalculateNextAction = true;
    }

    /***
     * Ustawia brakujace owoce i zaby na planszy
     */
    @Override
    public void performNextAction() {
        this.board.setMissingFrogs(this.missingFrogs);
        this.fruits_Frogs_Generator.generateFruitsAndFrogs(this.missingFruits, this.missingFrogs);
    }

    /***
     * Zwraca obiekt ktory ma zostac usuniety
     */
    @Override
    public ArrayList<Collisions> getGameObjectsToRemove() {
        return new ArrayList<>();
    }


    @Override
    public Collisions getRelatedGameObject() {
        return new Collisions() {
            @Override
            public String getName() {
                return "dummy";
            }
        };
    }


    @Override
    public void forceKill() {
        killed = true;
    }

}
