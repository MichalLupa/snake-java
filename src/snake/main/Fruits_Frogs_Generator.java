package snake.main;

import java.util.ArrayList;
import java.util.Random;

public class Fruits_Frogs_Generator {
    private final Board board;
    private final int fruitCount;
    private final int frogCount;
    private final Random rand;

    Fruits_Frogs_Generator(Board board, int fruitCount, int frogCount) {
        this.board = board;
        this.fruitCount = fruitCount;
        this.frogCount = frogCount;
        this.rand = new Random();
    }

    /***
     * Zwraca nieuzywane wspolrzedne
     */
    private Cords generateUnoccupiedCoordinates(int minX, int maxX, int minY, int maxY) {
        int xCoordinate = minX + rand.nextInt(maxX - minX);
        int yCoordinate = minY + rand.nextInt(maxY - minY);
        BoardTile[][] boardTiles = this.board.toTiles();

        while (boardTiles[xCoordinate][yCoordinate] != BoardTile.EMPTY) {
            xCoordinate = minX + rand.nextInt(maxX - minX);
            yCoordinate = minY + rand.nextInt(maxY - minY);
        }
        return new Cords(xCoordinate, yCoordinate);
    }

    /***
     * Zwraca liste owocow ktore trzeba dodac
     */
    public ArrayList<Fruit> generateMissingFruitsList() {
        int missingFruitsNumber = this.fruitCount - this.board.getFruits().size();
        ArrayList<Fruit> missingFruits = new ArrayList<>();
        for (int i = 0; i < this.rand.nextInt(Math.max(0, missingFruitsNumber + 1)); i++) {
            missingFruits.add(new Fruit(generateUnoccupiedCoordinates(0, this.board.getWidth(),
                    0, this.board.getHeight())));
        }
        return missingFruits;
    }

    /***
     * Zwraca liste zab ktore trzeba dodac
     */
    public ArrayList<Frog> generateMissingFrogsList() {
        int missingFrogsNumber = this.frogCount - this.board.getFrogs().size();
        ArrayList<Frog> missingFrogs = new ArrayList<>();
        for (int i = 0; i < this.rand.nextInt(Math.max(0, missingFrogsNumber + 1)); i++) {
            missingFrogs.add(new Frog(generateUnoccupiedCoordinates(0, this.board.getWidth(),
                    0, this.board.getHeight())));
        }
        return missingFrogs;
    }

    /***
     * Dodaje brakujace zaby i owoce na plansze
     * @param missingFruits lista owocow
     * @param missingFrogs lista zab
     */
    public void generateFruitsAndFrogs(ArrayList<Fruit> missingFruits, ArrayList<Frog> missingFrogs) {
        if (this.board.getFruits().size() < fruitCount) {
            try {
                for (Fruit missingFruit : missingFruits) {
                    this.board.addFruit(missingFruit);
                }
            } catch (TileOutOfBoundsException ignored) {

            }
        }
        if (this.board.getFrogs().size() < frogCount) {
            try {
                for (Frog missingFrog : missingFrogs) {
                    this.board.addFrog(missingFrog);
                }
            } catch (TileOutOfBoundsException ignored) {

            }
        }
    }
}
