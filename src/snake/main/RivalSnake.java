package snake.main;

import java.util.ArrayList;

public class RivalSnake extends Thread implements ObjectThread{
    private Board board;
    private SnakeAI snakeAI;
    private SnakeControls snakeControls;
    private boolean canCalculateNextAction = false;
    private Direction nextAction;
    private ArrayList<Collisions> gameObjectsToRemove = new ArrayList<>();
    private boolean killed = false;

    RivalSnake(Board board) {
        this.board = board;
        this.snakeAI = new SnakeAI(board, board.getEnemySnake());
        this.snakeControls = new SnakeControls(board, board.getEnemySnake());
    }

    /***
     * Oblicza kolejny ruch AI poprzez algorytm BFS 
     */
    @Override
    public void run() {
        while (true) {
            if (board.getEnemySnake() == null || killed) {
                break;
            }
            if (this.canCalculateNextAction) {
                this.nextAction = this.snakeAI.getNextMoveDirection();
                this.canCalculateNextAction = false;
            }
            try {
                Thread.sleep(1);
            } catch (Exception e) {
            }
        }
    }

    /***
     * Sprawdzenie kolejnego ruchu
     */
    @Override
    public void startCalculatingNextAction() {
        this.canCalculateNextAction = true;
    }

    /***
     * Ustawienie ruchu AI i sprawdzenie kolizji
     */
    @Override
    public void performNextAction() {
        try {
            this.board.getEnemySnake().setMoveDirection(this.nextAction);
        } catch (IncorrectDirectionException ignored) {
        }
        this.snakeControls.move();
        gameObjectsToRemove = snakeControls.handleCollisions();
    }

    /***
     * Zwraca obiekty do usuniecia
     */
    @Override
    public ArrayList<Collisions> getGameObjectsToRemove() {
        return gameObjectsToRemove;
    }

    /***
     * Zwraca AI 
     */
    @Override
    public Collisions getRelatedGameObject() {
        return board.getEnemySnake();
    }


    @Override
    public void forceKill() {
        killed = true;
    }
}
