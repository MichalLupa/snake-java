package snake.main;

import java.util.ArrayList;

public class FrogTh extends Thread implements ObjectThread {
    private Board board;
    private FrogAI frogAI;
    private boolean canCalculateNextAction = false;
    private Direction nextAction;
    private Frog frog;
    private ArrayList<Collisions> gameObjectsToRemove = new ArrayList<>();
    private boolean killed = false;

    FrogTh(Board board, Frog frog) {
        this.board = board;
        this.frogAI = new FrogAI(board, frog);
        this.frog = frog;
    }

    /***
     * Algorytm obliczajacy kolejny ruch zaby za pomaca BF
     */
    @Override
    public void run() {
        while (true) {
            if (!board.getFrogs().contains(frog) || killed) {
                frog = null;
                break;
            }
            if (this.canCalculateNextAction) {
                this.nextAction = this.frogAI.getNextMoveDirection();
                this.canCalculateNextAction = false;
            }
            try {
                Thread.sleep(1);
            } catch (Exception e) {
            }
        }
    }

    /***
     * Pozwala obliczyc kolejny ruch
     */
    @Override
    public void startCalculatingNextAction() {
        this.canCalculateNextAction = true;
    }

    /***
     * Ustawia kolejny ruch zaby i sprawdza czy jest kolizja
     */
    @Override
    public void performNextAction() {
        this.frog.setMoveDirection(this.nextAction);
        FrogController.move(this.frog);
        gameObjectsToRemove = FrogController.handleCollisions(frog, board);
    }


    @Override
    public ArrayList<Collisions> getGameObjectsToRemove() {
        return gameObjectsToRemove;
    }


    @Override
    public Collisions getRelatedGameObject() {
        return frog;
    }


    @Override
    public void forceKill() {
        killed = true;
    }
}
