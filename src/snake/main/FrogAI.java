package snake.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class FrogAI implements AI {
    private final Frog frog;
    private final Random rand;
    private final BFS bfs;

    FrogAI(Board board, Frog frog) {
        this.frog = frog;
        this.rand = new Random();
        this.bfs = new BFS(board);
    }

    /***
     * Pobiera kolejny ruch zaby poprzez algorytm BF lub losowo
     * @return zwraca kolejy ruch zaby
     */
    public Direction getNextMoveDirection() {
        Direction nextDirection;

        if (this.rand.nextInt(30) <= 1) {
            nextDirection = getRandomLegalDirection();
        } else {
            try {
                ArrayList<Cords> path = this.bfs.getLongestFrogPath(this.bfs.createFrogMatrix(this.frog), this.frog);
                Cords nextMove = path.get(1);
                Cords currentPosition = this.frog.getCoordinates();
                int[] deltas = {nextMove.x - currentPosition.x, nextMove.y - currentPosition.y};
                nextDirection = Direction_Utils.getDirection(deltas);
            } catch (Exception e) {
                nextDirection = getRandomLegalDirection();
            }
        }
        return nextDirection;
    }

    /***
     * Pobiera dozwolony ruch zaby 
     */
    private Direction getRandomLegalDirection() {
        ArrayList<Direction> directions = new ArrayList<>(List.of(Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT));
        Collections.shuffle(directions);
        for (Direction direction : directions) {
            int[] deltas = Direction_Utils.getDeltas(direction);
            int nextXCoordinate = this.frog.getCoordinates().x + deltas[0];
            int nextYCoordinate = this.frog.getCoordinates().y + deltas[1];
            if (this.bfs.areCoordinatesUnoccupiedAndInBoundsForFrog(new Cords(nextXCoordinate, nextYCoordinate))) {
                return direction;
            }
        }
        return Direction_Utils.getRandomDirection();
    }
}
