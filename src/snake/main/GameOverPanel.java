package snake.main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/***
 * Panel po smierci gracza
 */
public class GameOverPanel extends JPanel implements ActionListener{
    JButton menuButton = new JButton("Back to Menu");
    JButton saveButton = new JButton("Save");
    JLabel gameOverLabel = new JLabel();
    JLabel infoLabel = new JLabel();
    JLabel scoreLabel = new JLabel();
    JTextField nameTextField = new JTextField();
    private int score = 0;
    private Ranking records;

    /***
     * GameOverPanel konstruktor
     * @param actionListener Sprawdzenie ktore opcje zostalay wybrane
     */
    GameOverPanel(ActionListener actionListener) {
    	menuButton.setBackground(Color.LIGHT_GRAY); 
        menuButton.setActionCommand("MENU");
        saveButton.setActionCommand("SAVE");
        saveButton.setBackground(Color.LIGHT_GRAY);
        gameOverLabel.setText("Game Over");
        infoLabel.setText("Podaj swoj� nazw�: ");

        GameFrame gameFrame = ((GameFrame)actionListener);

        JLabel[] jlabels = {gameOverLabel, scoreLabel, infoLabel};
        for (JLabel jlabel: jlabels) {
            jlabel.setHorizontalAlignment(JLabel.CENTER);
            add(jlabel);
            jlabel.setVisible(true);
        }

        nameTextField.setVisible(true);
        add(nameTextField);
        nameTextField.setHorizontalAlignment(JTextField.CENTER);

        JButton[] buttons = {saveButton, menuButton};
        for (JButton button: buttons) {
            button.setHorizontalAlignment(JButton.CENTER);
            button.setVisible(true);
            if (button != saveButton) {
                button.addActionListener(actionListener);
            }
            add(button);
        }
        saveButton.addActionListener(this);

        gameOverLabel.setFont(new Font("default", Font.BOLD, (int)(gameFrame.getWidth()*0.065)));

        setLayout(new GridLayout(7, 1, 0, (int)(gameFrame.getHeight()*0.05)));
        setBorder(BorderFactory.createEmptyBorder((int)(gameFrame.getHeight()*0.05), (int)(gameFrame.getWidth()*0.1),
                (int)(gameFrame.getHeight()*0.05), (int)(gameFrame.getWidth()*0.1)));
    }

    /***
     * Reakcja na wcisniety przycisk
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("SAVE")) {
            String name = "?";
            if (!nameTextField.getText().equals("")) {
                name = nameTextField.getText();
            }
            name = name.replaceAll("\\s+", "");
            Ranking.HighScoreRecord record = new Ranking.HighScoreRecord(name, score);
            records.addRecord(record);
            records.writeToFile();
            saveButton.setEnabled(false);
            menuButton.setEnabled(true);
        }
    }

    /***
     * Pozwala na dostep do wynikow 

     */
    public void setScore(int score) {
        this.score = score;
        scoreLabel.setText("Score: " + score);
    }

    /***
     * Pozwala na dostep do najwyzszych wynikow w celu zapisania nowego
     */
    public void setHighScoreRecords(Ranking records) {
        this.records = records;

    }

    /***
     * Resets the state of GameOverPanel for next use
     */
    public void reset() {
        saveButton.setEnabled(true);
        saveButton.setEnabled(true);
        menuButton.setEnabled(false);
        nameTextField.setText("");
    }

}
