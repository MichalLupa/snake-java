package snake.main;

import java.util.ArrayList;
import java.io.*;
import java.util.Comparator;

/***
 * Spakowana lista wynikow
 */
public class Ranking {
    /***
     * Najwyzsze wyniki graczy i ich nazwya
     */
    public static class HighScoreRecord {
        private final String name;
        private final int score;

        /***
         * HighScoreRecord konstruktor
         * @param name nazwa gracza
         * @param score wynik gracza
         */
        HighScoreRecord(String name, int score) {
            this.name = name;
            this.score = score;
        }

        /***
         * Pobiera wynik
         * @return wynik danej rozgrywki
         */
        public int getScore() {
            return score;
        }

        /***
         * @return sformatowany wynik z nazwa jako string
         */
        @Override
        public String toString() {
            return (name + " " + score);
        }
    }

    private ArrayList<HighScoreRecord> records = new ArrayList<>();
    private final String saveFileName;


    Ranking(String saveFileName) {
        this.saveFileName = saveFileName;
    }

    /***
     * Pobiera liste wynikow
     * @return zwracana lista wynikow
     */
    public ArrayList<HighScoreRecord> getRecords() {
        return records;
    }

    /***
     * Dodaje wynik do listy wynikow
     * @param dodawany wynik
     */
    public void addRecord(HighScoreRecord record) {
        records.add(record);
        records.sort(Comparator.comparingInt(HighScoreRecord::getScore).reversed());
    }

    /***
     * Wczytuje wyniki
     */
    public void readFromFile() {
        File inputFile = new File(saveFileName).getAbsoluteFile();
        FileInputStream inputStream;
        try {
            inputStream = new FileInputStream(inputFile);
        }
        catch (Exception e) {
            return;
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        String line;

        ArrayList<HighScoreRecord> readRecords = new ArrayList<>();

        do {
            try {
                line = bufferedReader.readLine();
            }
            catch (IOException e) {
                break;
            }
            if (line == null) {
                break;
            }
            String[] possibleRecord = line.split(" ");
            if (possibleRecord.length != 2) {
                continue;
            }
            String name = possibleRecord[0];
            int score = Integer.parseInt(possibleRecord[1]);
            HighScoreRecord highScoreRecord = new HighScoreRecord(name, score);
            readRecords.add(highScoreRecord);

        } while (line != null);

        records = readRecords;

        if (!records.isEmpty()) {
            records.sort(Comparator.comparingInt(HighScoreRecord::getScore).reversed());
        }

        try {
            bufferedReader.close();
            inputStream.close();
        }
        catch (IOException e) {}
    }

    /***
     * Zapisuje wyniki
     */
    public void writeToFile() {
        File outputFile = new File(saveFileName);
        FileOutputStream outputStream;
        if (!outputFile.exists()) {
            try {
                outputFile.createNewFile();
            }
            catch (Exception e) {
                System.out.println("Failed to save score");
                return;
            }
        }
        try {
            outputStream = new FileOutputStream(outputFile, false);
        }
        catch (Exception e) {
            System.out.println("Failed to save score");
            return;
        }

        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));

        for (HighScoreRecord record: records) {
            try {
                bufferedWriter.write(record.toString());
                bufferedWriter.newLine();
            }
            catch (IOException e) {
                break;
            }
        }


        try {
            bufferedWriter.flush();
            bufferedWriter.close();
        }
        catch (IOException e) {}
    }
}
