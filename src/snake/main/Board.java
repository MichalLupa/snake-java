package snake.main;

import java.util.ArrayList;
import java.util.Arrays;

/***
 * Dostepne obiekty na planszy
 */
enum BoardTile {
    EMPTY,
    ENEMY_SNAKE,
    ENEMY_SNAKE_HEAD,
    FROG,
    SNAKE,
    SNAKE_HEAD,
    FRUIT,
    OBSTACLE 
}

/***
 * Exception jesli jest poza plansza
 */
class TileOutOfBoundsException extends Exception {}

/***
 * Klasa reprezentujaca plansze do gry
 */
public class Board {
    /***
     * Spakowane w liste obiekty gry
     */
    public static class GameObjectArrayList {
        public ArrayList<Collisions> gameObjects = new ArrayList<>();
    }
    private Snake playerSnake;
    private Snake enemySnake;
    private final int width;
    private final int height; 
    private final ArrayList<Cords> obstacles = new ArrayList<>();
    private ArrayList<Fruit> fruits = new ArrayList<>();
    private ArrayList<Frog> frogs = new ArrayList<>();
    private ArrayList<Frog> missingFrogs = new ArrayList<>();

    /***
     * Konstruktor planszy
     * @param width szereokosc planszy
     * @param height wysokosc planszy
     */
    public Board(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /***
     * Zwraza szerokosc planszy
     */
    public int getWidth() {
        return width;
    }

    /***
     * Zwraca wysokosc planszy
     */
    public int getHeight() {
        return height;
    }

    /***
     * Spprawdza czy wspolrzedne mieszcza sie na planszy
     * @param wspolrzedne do sprawdzenia
     */
    private boolean IsOutOfBounds(Cords cords) {
        return cords.x > this.width || cords.x < 0 || cords.y > this.height || cords.y < 0;
    }

    /***
     * Returns the snake controlled by the player
     * @return player controlled snake
     */
    public Snake getPlayerSnake() {
        return playerSnake;
    }

    /***
     * Ustawia weza gracza
     */
    public void setPlayerSnake(Snake playerSnake) throws TileOutOfBoundsException {
        if (IsOutOfBounds(playerSnake.getSnakeHead())) {
            throw new TileOutOfBoundsException();
        }
        for (Cords snakeTile: playerSnake.getSnakeBody()) {
            if (IsOutOfBounds(snakeTile)) {
                throw new TileOutOfBoundsException();
            }
        }

        this.playerSnake = playerSnake;
    }

    public Snake getEnemySnake() {
        return enemySnake;
    }

    /***
     * Ustawia AI
     */
    public void setEnemySnake(Snake enemySnake) throws TileOutOfBoundsException {
        if (IsOutOfBounds(enemySnake.getSnakeHead())) {
            throw new TileOutOfBoundsException();
        }
        for (Cords enemySnakeTile: enemySnake.getSnakeBody()) {
            if (IsOutOfBounds(enemySnakeTile)) {
                throw new TileOutOfBoundsException();
            }
        }

        this.enemySnake = enemySnake;
    }

    public ArrayList<Frog> getFrogs() {
        return this.frogs;
    }

    /***
     * Dodaje zabe do listy 
     */
    public void addFrog(Frog frog) throws TileOutOfBoundsException{
        if (IsOutOfBounds(frog.getCoordinates())) {
            throw new TileOutOfBoundsException();
        }
        this.frogs.add(frog);
    }

    /***
     * Dodaje owoce do listy
     */
    public void addFruit(Fruit fruit) throws TileOutOfBoundsException{
        if (IsOutOfBounds(fruit.getCoordinates())) {
            throw new TileOutOfBoundsException();
        }
        this.fruits.add(fruit);
    }

    /***
     * Zwraca liste brakujacyhc zab
     */
    public ArrayList<Frog> getMissingFrogs() {
        return this.missingFrogs;
    }

    /***
     * Zwraca liste brakujacych owocow
     */
    public void setMissingFrogs(ArrayList<Frog> frogs) {
        this.missingFrogs = frogs;
        this.frogs.addAll(this.missingFrogs);
    }

    /***
     * Czysci liste brakujacych zab
     */
    public void clearMissingFrogs() {
        this.missingFrogs.clear();
    }

    /***
     * Usuwa obiekt z gry
     */
    public void removeGameObject(Collisions gameObject) {
        switch (gameObject.getName()) {
            case "Fruit":
                fruits.remove(((Fruit) gameObject));
                break;
            case "Frog":
                frogs.remove(((Frog) gameObject));
                break;
            case "Snake":
                if (gameObject == playerSnake) {
                    playerSnake = null;
                }
                else {
                    enemySnake = null;
                }
                break;
            default:
                break;
        }
    }

    /***
     * Zwraca liste obiektow na planszy
     */
    public ArrayList<Fruit> getFruits() {
        return this.fruits;
    }

    /***
     * Dodaje przeszkode do gry
     */
    public void addObstacle(Cords obstacle) throws TileOutOfBoundsException{
        this.obstacles.add(obstacle);
    }

    /***
     * Zwraca plansze jako tablice dwuwymiarowa
     */
    public BoardTile[][] toTiles() {
        BoardTile[][] tiles = new BoardTile[this.width][this.height];

        for (BoardTile[] row: tiles) {
            Arrays.fill(row, BoardTile.EMPTY);
        }

        for (Frog frog: this.frogs) {
            tiles[frog.getCoordinates().x][frog.getCoordinates().y] = BoardTile.FROG;
        }

        for (Fruit fruit: fruits) {
            tiles[fruit.getCoordinates().x][fruit.getCoordinates().y] = BoardTile.FRUIT;
        }

        if (this.playerSnake != null) {
            ArrayList<Cords> snakeBody = playerSnake.getSnakeBody();
            for (Cords snakeTile: snakeBody) {
                tiles[snakeTile.x][snakeTile.y] = BoardTile.SNAKE;
            }
            tiles[playerSnake.getSnakeHead().x][playerSnake.getSnakeHead().y] = BoardTile.SNAKE_HEAD;
        }

        if (this.enemySnake != null) {
            ArrayList<Cords> enemySnakeBody = enemySnake.getSnakeBody();
            for (Cords enemySnakeTile: enemySnakeBody) {
                tiles[enemySnakeTile.x][enemySnakeTile.y] = BoardTile.ENEMY_SNAKE;
            }
            tiles[enemySnake.getSnakeHead().x][enemySnake.getSnakeHead().y] = BoardTile.ENEMY_SNAKE_HEAD;
        }

        if (!this.obstacles.isEmpty()){
            for (Cords obstacle : obstacles) {
                tiles[obstacle.x][obstacle.y] = BoardTile.OBSTACLE;
            }
        }

        return tiles;
    }

    /***
     * Zwraca liste uzywanych wspolrzednych na planszy
     */
    public GameObjectArrayList[][] getReferenceMatrix(Collisions currentGameObject) {
        GameObjectArrayList[][] matrix = new GameObjectArrayList[this.width][this.height];


        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                matrix[i][j] = new GameObjectArrayList();
            }
        }

        if (this.playerSnake != null) {
            try {
                if (currentGameObject != playerSnake) {
                    matrix[playerSnake.getSnakeHead().x][playerSnake.getSnakeHead().y].gameObjects.add(playerSnake);
                }
                ArrayList<Cords> snakeBody = playerSnake.getSnakeBody();
                for (Cords snakeTile : snakeBody) {
                    matrix[snakeTile.x][snakeTile.y].gameObjects.add(playerSnake);
                }
            }
            catch (Exception e) { }
        }

        if (this.enemySnake != null) {
            try {
                if (currentGameObject != enemySnake) {
                    matrix[enemySnake.getSnakeHead().x][enemySnake.getSnakeHead().y].gameObjects.add(enemySnake);
                }
                ArrayList<Cords> enemySnakeBody = enemySnake.getSnakeBody();
                for (Cords enemySnakeTile : enemySnakeBody) {
                    matrix[enemySnakeTile.x][enemySnakeTile.y].gameObjects.add(enemySnake);
                }
            }
            catch (Exception e) { }
        }

        if (!this.frogs.isEmpty()){
            for (Frog frog: this.frogs) {
                try {
                    if (currentGameObject != frog) {
                        matrix[frog.getCoordinates().x][frog.getCoordinates().y].gameObjects.add(frog);
                    }
                }
                catch (Exception e) { }
            }
        }

        if (!this.fruits.isEmpty()){
            for (Fruit fruit: fruits) {
                matrix[fruit.getCoordinates().x][fruit.getCoordinates().y].gameObjects.add(fruit);
            }
        }

        if (!this.obstacles.isEmpty()){
            for (Cords obstacle : obstacles) {
                matrix[obstacle.x][obstacle.y].gameObjects.add(obstacle);
            }
        }

        return matrix;
    }
}
