package snake.main;

import java.util.ArrayList;

/***
 * Klasa reprezentujaca weza, zawiera glowe, cialo i ruch w danym kierunku
 */
public class Snake implements Collisions{
    private ArrayList<Cords> snakeBody;
    private Cords snakeHead;
    private Direction moveDirection;

    /***
     * Snake Konstruktor
     * @param snakeBody lista zawierajaca dlugosc ciala
     * @param snakeHead wspolrzedne glowy weza
     * @param direction kierunek ruchu
     */
    Snake(ArrayList<Cords> snakeBody, Cords snakeHead, Direction direction) {
        this.snakeBody = new ArrayList<>();
        for (Cords cords : snakeBody) {
            this.snakeBody.add(new Cords(cords.x, cords.y));
        }
        this.snakeHead = new Cords(snakeHead.x, snakeHead.y);
        this.moveDirection = direction;
    }

    /***
     * @return lista zawierajaca dlugosc ciala
     */
    public ArrayList<Cords> getSnakeBody() {
        return this.snakeBody;
    }

    /***
     * @return glowa weza
     */
    public Cords getSnakeHead() {
        return this.snakeHead;
    }

    /***
     * Ruch weza
     * @param moveDirection kierunek weza
     * @throws IncorrectDirectionException jesli ruch w danym kierunku nie jest mozliwy
     */
    public void setMoveDirection(Direction moveDirection) throws IncorrectDirectionException {
        if (this.moveDirection == Direction_Utils.getOppositeDirection(moveDirection)) {
            throw new IncorrectDirectionException();
        }
        this.moveDirection = moveDirection;
    }

    /***
     * @return ruch w danym kierunku
     */
    public Direction getMoveDirection() {
        return moveDirection;
    }

    /***
     * @return zwraca obiekt weza w postaci stringa
     */
    public String getName() {
        return "Snake";
    }
}
