package snake.main;

import java.util.ArrayList;

/***
 * Klasa uzywana do kontrolowania ruchu weza
 */
public class SnakeControls {
    private Board board;
    private Snake snake;
    private boolean nextMoveGrows;

    /***
     * SnakeController konstruktor
     * @param board plansza
     * @param snake dany waz
     */
    SnakeControls(Board board, Snake snake) {
        this.board = board;
        this.snake = snake;
        this.nextMoveGrows = false;
    }

    /***
     * Ruch wezem w danym kierunku i sprawdzenie czy zjadl owoc lub zabe
     */
    public void move() {
        if (!nextMoveGrows) {
            normalMove();
        } else {
            growingMove();
            nextMoveGrows = false;
        }
    }

    /***
     * Sprawdzenie czy nastapila kolizja z obiektem
     * @return usuwany obiekt w zaleznosci od kolizji
     */
    public ArrayList<Collisions> handleCollisions() {
        Cords head = snake.getSnakeHead();
        ArrayList<Collisions> gameObjectsToRemove = new ArrayList<>();
        if ( head.x < 0 || head.x >= board.getWidth() ||
                head.y < 0 || head.y >= board.getHeight()) {
            gameObjectsToRemove.add(snake);
        }
        else {
            Board.GameObjectArrayList[][] references = board.getReferenceMatrix(snake);
            ArrayList<Collisions> gameObjects = references[head.x][head.y].gameObjects;
            if (!gameObjects.isEmpty()) {
                for (Collisions gameObject: gameObjects) {
                    switch (gameObject.getName()) {
                        case "Coordinates": case "Snake":
                            gameObjectsToRemove.add(snake);
                            break;
                        case "Frog": case "Fruit":
                            this.nextMoveGrows = true;
                            gameObjectsToRemove.add(gameObject);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        return gameObjectsToRemove;
    }

    private void normalMove() {
        Cords snakeHead = snake.getSnakeHead();
        ArrayList<Cords> snakeBody = snake.getSnakeBody();
        Direction moveDirection = snake.getMoveDirection();
        for (int i = (snakeBody.size() - 1); i >= 0; i-- ) {
            if (i == 0) {
                snakeBody.get(i).x = snakeHead.x;
                snakeBody.get(i).y = snakeHead.y;
            }
            else {
                snakeBody.get(i).x = snakeBody.get(i - 1).x;
                snakeBody.get(i).y = snakeBody.get(i - 1).y;
            }
        }
        snakeHead.x += Direction_Utils.getDeltas(moveDirection)[0];
        snakeHead.y += Direction_Utils.getDeltas(moveDirection)[1];
    }

    private void growingMove() {
        Cords snakeHead = snake.getSnakeHead();
        ArrayList<Cords> snakeBody = snake.getSnakeBody();
        Direction moveDirection = snake.getMoveDirection();
        Cords lastSegment = snakeBody.get(snakeBody.size() - 1);
        Cords segmentToAdd = new Cords(lastSegment.x, lastSegment.y);
        for (int i = (snakeBody.size() - 1); i >= 0; i-- ) {
            if (i == 0) {
                snakeBody.get(i).x = snakeHead.x;
                snakeBody.get(i).y = snakeHead.y;
            }
            else {
                snakeBody.get(i).x = snakeBody.get(i - 1).x;
                snakeBody.get(i).y = snakeBody.get(i - 1).y;
            }
        }
        snakeBody.add(segmentToAdd);
        snakeHead.x += Direction_Utils.getDeltas(moveDirection)[0];
        snakeHead.y += Direction_Utils.getDeltas(moveDirection)[1];
    }
}
