package snake.main;

import java.util.ArrayList;

public class PlayerSnakeThread extends Thread implements ObjectThread {
    private Board board;
    private SnakeAI snakeAI;
    private SnakeControls snakeControls;
    private boolean canCalculateNextAction = false;
    private Direction nextAction;
    private KeyboardControler keyboardControler;
    private ArrayList<Collisions> gameObjectsToRemove = new ArrayList<>();
    private boolean killed = false;

    PlayerSnakeThread(Board board, KeyboardControler keyboardControler) {
        this.board = board;
        this.snakeAI = new SnakeAI(board, board.getPlayerSnake());
        this.snakeControls = new SnakeControls(board, board.getPlayerSnake());
        this.keyboardControler = keyboardControler;
    }

    /***
     * ruch weza za pomoca klawiatury
     */
    @Override
    public void run() {
        while (true) {
            if (board.getPlayerSnake() == null || killed) {
                break;
            }
            if (this.canCalculateNextAction) {
                this.nextAction = this.board.getPlayerSnake().getMoveDirection();
                switch (this.keyboardControler.getRecentlyPressedKey()) {
                    case UP:
                        this.nextAction = Direction.UP;
                        break;
                    case DOWN:
                        this.nextAction = Direction.DOWN;
                        break;
                    case LEFT:
                        this.nextAction = Direction.LEFT;
                        break;
                    case RIGHT:
                        this.nextAction = Direction.RIGHT;
                        break;
                    default:
                        break;
                }
                this.canCalculateNextAction = false;
            }
            try {
                Thread.sleep(1);
            } catch (Exception e) {
            }
        }
    }


    @Override
    public void startCalculatingNextAction() {
        this.canCalculateNextAction = true;
    }

    /***
     * Ustawia kierunek ruchu i sprawdza czy nastapi kolizja
     */
    @Override
    public void performNextAction() {
        try {
            this.board.getPlayerSnake().setMoveDirection(this.nextAction);
        }
        catch (IncorrectDirectionException ignored) {
        }
        this.snakeControls.move();
        gameObjectsToRemove = snakeControls.handleCollisions();
    }

    /***
     * Usuniecie watkow objektow gry
     * @return obiekty do usuniecia
     */
    @Override
    public ArrayList<Collisions> getGameObjectsToRemove() {
        return gameObjectsToRemove;
    }

    /***
     * @return zwracany waz
     */
    @Override
    public Collisions getRelatedGameObject() {
        return board.getPlayerSnake();
    }

    /***
     * Ustawienie natychmiastowego usunieca watku
     */
    @Override
    public void forceKill() {
        killed = true;
    }
}
