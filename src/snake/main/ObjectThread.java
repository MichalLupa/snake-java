package snake.main;

import java.util.ArrayList;

public interface ObjectThread {
    /***
     * Start
     */
    void start();

 
    void startCalculatingNextAction();


    void performNextAction();


    ArrayList<Collisions> getGameObjectsToRemove();


    Collisions getRelatedGameObject();


    void forceKill();
}
