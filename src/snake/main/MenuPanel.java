package snake.main;

import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;

/***
 * Wyswietlenie menu
 */
public class MenuPanel extends JPanel{
    JLabel titleLabel = new JLabel("Gra Snake");
    JButton startButton = new JButton("Start");
    JButton highScoreButton = new JButton("Ranking");

    /***
     * MenuPanel konstruktor
     * @param actionListener Sprawdzenie ktore opcje zostalay wybrane
     */
    MenuPanel(ActionListener actionListener) {
        GameFrame gameFrame = (GameFrame) actionListener;
        
        
        
        
        titleLabel.setFont(new Font("default", Font.BOLD, (int)(gameFrame.getWidth()*0.065)));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        add(titleLabel);

        startButton.setActionCommand("START");
        startButton.setBackground(Color.LIGHT_GRAY);
        highScoreButton.setActionCommand("RANKING");
        highScoreButton.setBackground(Color.LIGHT_GRAY);
        
        JButton[] buttons = {startButton, highScoreButton};
        for (JButton button: buttons) {
            button.setVisible(true);
            add(button);
            button.addActionListener(actionListener);
        }

        setLayout(new GridLayout(4, 1, 0, (int)(gameFrame.getHeight()*0.05)));
        setBorder(BorderFactory.createEmptyBorder((int)(gameFrame.getHeight()*0.05), (int)(gameFrame.getWidth()*0.1),
                (int)(gameFrame.getHeight()*0.05), (int)(gameFrame.getWidth()*0.1)));

        this.setAlignmentY(CENTER_ALIGNMENT);
    }
}
