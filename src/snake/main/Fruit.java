package snake.main;

/***
 * Klasa reprezentujaca owoce, zawiera ich pojedyncze wspolrzedne
 */
public class Fruit implements Collisions{
    private Cords cords;

    /**
     * Fruit konstruktor
     * @param cords wspolrzedne do wstawienia owocu
     */
    Fruit(Cords cords) {
        this.cords = new Cords(cords.x, cords.y);
    }

    /***
     * Zwraca wspolrzedne polozenia owocu
     */
    public Cords getCoordinates() {
        return this.cords;
    }

    /***
     * Ustawia owoc
     */
    public void setCoordinates(Cords cords) {
        this.cords.x = cords.x;
        this.cords.y = cords.y;
    }

    public String getName() {
        return "Fruit";
    }
}
