package snake.main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/***
 * Menu uzywane do wyswietlenia wynikow .
 */
public class RankingPanel extends JPanel implements ActionListener{
    JButton menuButton = new JButton("Back to Menu");
    JList<String> highScoreRecordList = new JList<>();
    Ranking records;
    GameFrame gameFrame;

    /***
     * @param actionListener Sprawdzenie ktore opcje zostalay wybrane
     */
    RankingPanel(ActionListener actionListener) {
        gameFrame = ((GameFrame)actionListener);
        menuButton.setBackground(Color.LIGHT_GRAY);
        JPanel upperPanel = new JPanel();
        JPanel lowerPanel = new JPanel();
        lowerPanel.setBorder(BorderFactory.createEmptyBorder((int)(gameFrame.getHeight()*0.05),
                (int)(gameFrame.getWidth()*0.1), (int)(gameFrame.getHeight()*0.05), (int)(gameFrame.getWidth()*0.1)));
        lowerPanel.setVisible(true);
        upperPanel.setVisible(true);
        lowerPanel.setLayout(new GridLayout(2, 1, 0, (int)(gameFrame.getHeight()*0.05)));

        setLayout(new GridLayout(2, 1, 0, (int)(gameFrame.getHeight()*0.05)));
        setBorder(BorderFactory.createEmptyBorder((int)(gameFrame.getHeight()*0.05), (int)(gameFrame.getWidth()*0.1),
                (int)(gameFrame.getHeight()*0.05), (int)(gameFrame.getWidth()*0.1)));


        highScoreRecordList.setVisible(true);
        upperPanel.add(highScoreRecordList);

        menuButton.setActionCommand("MENU");
        menuButton.addActionListener(actionListener);
        menuButton.setVisible(true);
        lowerPanel.add(menuButton);
        add(upperPanel);
        add(lowerPanel);
    }

    /***
     * Reakcja na wcisniety przycisk

     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("CLEAR")) {
            records.getRecords().clear();
            records.writeToFile();
            gameFrame.actionPerformed(new ActionEvent(gameFrame, ActionEvent.ACTION_PERFORMED, "HIGH_SCORE"));
        }
    }


    public void setHighScoreRecords(Ranking records) {
        this.records = records;
        if (records != null) {
            DefaultListModel<String> listModel = new DefaultListModel<>();
            for (Ranking.HighScoreRecord record: records.getRecords()) {
                listModel.addElement(record.toString());
            }
            highScoreRecordList.setModel(listModel);
        }
    }

}
