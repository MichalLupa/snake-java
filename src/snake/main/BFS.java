package snake.main;

import java.util.*;

public class BFS {
    private final Board board;
    private final BoardTile[][] boardTiles;

    BFS(Board board) {
        this.board = board;
        this.boardTiles = this.board.toTiles();
    }

    /***
     * Tworzy najkrotsza trase do zaby lub owocu ppoprzez algorytm BF
     * @param startingPoint wspolrzedne weza
     * @return trasa od glowy do najblizszego owocu, lub zaby
     */
    public ArrayList<Cords> getShortestPathToFrogOrFruit(Cords startingPoint) {
        Cords targetCoordinates = null;
        boolean[][] visitedCoordinates = new boolean[board.getWidth()][board.getHeight()];
        Cords[][] previousCoordinates = new Cords[board.getWidth()][board.getHeight()];
        Queue<Cords> boardCoordinatesQueue = new LinkedList<>();

        boardCoordinatesQueue.add(startingPoint);
        visitedCoordinates[startingPoint.x][startingPoint.y] = true;

        while (!boardCoordinatesQueue.isEmpty()) {
            Cords currentCoordinates = boardCoordinatesQueue.poll();
            if (boardTiles[currentCoordinates.x][currentCoordinates.y] == BoardTile.FRUIT) {
                targetCoordinates = currentCoordinates;
                break;
            } else if (boardTiles[currentCoordinates.x][currentCoordinates.y] == BoardTile.FROG) {
                targetCoordinates = currentCoordinates;
                break;
            }
            for (Cords neighbor : getNeighboringCoordinates(currentCoordinates,BoardTile.ENEMY_SNAKE_HEAD)) {
                if (!visitedCoordinates[neighbor.x][neighbor.y]) {
                    visitedCoordinates[neighbor.x][neighbor.y] = true;
                    boardCoordinatesQueue.add(neighbor);
                    previousCoordinates[neighbor.x][neighbor.y] = currentCoordinates;
                }
            }
        }
        return recreatePath(previousCoordinates, this.board.getEnemySnake().getSnakeHead(), targetCoordinates);
    }

    /***
     * Tworzy dwuwymiarowa tablice reprezentujaca liczbe ruchow do danej kratki na planszy
     * @param startingPoints lista punktow polozenia od ktorych obliza się liczbe ruchow
     * @param boardTile mowi czy dany punkt jest zaba czy wezem
     * @return dwuwymiarowa tablica zawierajaca odleglosci
     */
    int[][] getDistancesMatrix(ArrayList<Cords> startingPoints, BoardTile boardTile) {
        boolean[][] visitedCoordinates = new boolean[this.board.getWidth()][this.board.getHeight()];
        Queue<Cords> boardCoordinatesQueue = new LinkedList<>();
        int[][] distancesMatrix = new int[this.board.getWidth()][this.board.getHeight()];

        for (Cords startingPoint : startingPoints) {
            boardCoordinatesQueue.add(startingPoint);
            visitedCoordinates[startingPoint.x][startingPoint.y] = true;
        }
        while (!boardCoordinatesQueue.isEmpty()) {
            Cords currentCoordinates = boardCoordinatesQueue.poll();
            for (Cords neighbor : getNeighboringCoordinates(currentCoordinates, boardTile)) {
                if (!visitedCoordinates[neighbor.x][neighbor.y]) {
                    visitedCoordinates[neighbor.x][neighbor.y] = true;
                    distancesMatrix[neighbor.x][neighbor.y] = distancesMatrix[currentCoordinates.x][currentCoordinates.y] + 1;
                    boardCoordinatesQueue.add(neighbor);
                }
            }
        }

        return distancesMatrix;
    }

    /***
     * tworzy najdluzsza mozliwa trase ucieczki zaby poprzez algorytm BF
     * @param frogMatrix dwuwymiarowa tablica zawierajaca odleglosci zaby od glowy weza
     * @param frog zaba dla ktorej obliczamy trase
     * @return najdluzsza mozliwa trasa ucieczki zaby
     */
    public ArrayList<Cords> getLongestFrogPath(int[][] frogMatrix, Frog frog) {
        Cords targetCoordinates = null;
        boolean[][] visitedCoordinates = new boolean[board.getWidth()][board.getHeight()];
        Cords[][] previousCoordinates = new Cords[board.getWidth()][board.getHeight()];
        Queue<Cords> boardCoordinatesQueue = new LinkedList<>();
        Cords startingPoint = frog.getCoordinates();
        boardCoordinatesQueue.add(startingPoint);
        visitedCoordinates[startingPoint.x][startingPoint.y] = true;
        previousCoordinates[startingPoint.x][startingPoint.y] = startingPoint;

        while (!boardCoordinatesQueue.isEmpty()) {
            Cords currentCoordinates = boardCoordinatesQueue.poll();
            for (Cords neighbor : getNeighboringCoordinates(currentCoordinates, BoardTile.FROG)) {
                if (!visitedCoordinates[neighbor.x][neighbor.y] && frogMatrix[neighbor.x][neighbor.y] > 0) {
                    visitedCoordinates[neighbor.x][neighbor.y] = true;
                    boardCoordinatesQueue.add(neighbor);
                    previousCoordinates[neighbor.x][neighbor.y] = currentCoordinates;
                }
            }
            targetCoordinates = currentCoordinates;
        }
        return recreatePath(previousCoordinates, startingPoint, targetCoordinates);
    }

    /***
     * Tworzy liste dostepnych wspolrzednych obok obiektu
     * @param wspolrzedne danego obiektu
     * @param boardTile typ danego obiektu
     * @return lista dostepnych wspolrzednych
     */
    private ArrayList<Cords> getNeighboringCoordinates(Cords cords, BoardTile boardTile) {
        ArrayList<Cords> potentialNeighbors = new ArrayList<>();
        ArrayList<Cords> neighbors = new ArrayList<>();
        potentialNeighbors.add(new Cords(cords.x , cords.y - 1));
        potentialNeighbors.add(new Cords(cords.x , cords.y + 1));
        potentialNeighbors.add(new Cords(cords.x - 1, cords.y));
        potentialNeighbors.add(new Cords(cords.x + 1, cords.y));

        for (Cords potentialNeighbor : potentialNeighbors) {
            if (areCoordinatesUnoccupiedAndInBounds(potentialNeighbor, boardTile)) {
                neighbors.add(potentialNeighbor);
            }
        }
        return neighbors;
    }

    /***
     * Sprawdzenie czy mozna umiescic zabe lub weza na danych wspolrzednych
     * @param cords sprawdzane wspolrzedne
     * @param boardTile typ obiektu czy jest to zaba czy waz
     * @return zwraca True jesli mozna umiescic dany obiekt i False jeśli nie mozna
     */
    public boolean areCoordinatesUnoccupiedAndInBounds(Cords cords, BoardTile boardTile) {
        boolean output = false;
        if (boardTile == BoardTile.ENEMY_SNAKE_HEAD) {
            output = areCoordinatesUnoccupiedAndInBoundsForSnake(cords);
        } else if (boardTile == BoardTile.FROG) {
            output = areCoordinatesUnoccupiedAndInBoundsForFrog(cords);
        }
        return output;
    }

    /***
     * sprawdzenie czy mozna umiescic obiekt obok weza
     * @param cords sprawdzane wspolrzede
     * @return zwraca True jesli mozna umiescic obiekt i False jesli nie
     */
    public boolean areCoordinatesUnoccupiedAndInBoundsForSnake(Cords cords) {
        ArrayList<BoardTile> obstacles = new ArrayList<>(List.of(
                BoardTile.SNAKE, BoardTile.SNAKE_HEAD, BoardTile.ENEMY_SNAKE, BoardTile.ENEMY_SNAKE_HEAD, BoardTile.OBSTACLE));
        return cords.x >= 0 && cords.y >= 0 &&
                cords.x < this.board.getWidth() && cords.y < this.board.getHeight() &&
                !obstacles.contains(this.boardTiles[cords.x][cords.y]);
    }

    /***
     * sprawdzenie czy mozna umiescic obiekt obok zaby
     * @param cords sprawdzane wspolrzede
     * @return zwraca True jesli mozna umiescic obiekt i False jesli nie
     */
    public boolean areCoordinatesUnoccupiedAndInBoundsForFrog(Cords cords) {
        ArrayList<BoardTile> obstacles = new ArrayList<>(List.of(
                BoardTile.SNAKE, BoardTile.SNAKE_HEAD,
                BoardTile.ENEMY_SNAKE, BoardTile.ENEMY_SNAKE_HEAD, BoardTile.OBSTACLE, BoardTile.FRUIT, BoardTile.FROG));
        return cords.x >= 0 && cords.y >= 0 &&
                cords.x < this.board.getWidth() && cords.y < this.board.getHeight() &&
                !obstacles.contains(this.boardTiles[cords.x][cords.y]);
    }

    /***
     * Tworzy sciezke z listy wspolrzednych
     * @param previous lista wspolrzednych
     * @param startingCoordinates wspolrzedne poczatkowe sciezki
     * @param targetCoordinates wspolrzedne docelowe sciezki
     * @return sciezka
     */
    private ArrayList<Cords> recreatePath(Cords[][] previous, Cords startingCoordinates,
                                                Cords targetCoordinates) {
        ArrayList<Cords> path = new ArrayList<>();
        Cords currentCoordinates = targetCoordinates;
        path.add(currentCoordinates);
        while (currentCoordinates != startingCoordinates) {
            currentCoordinates = previous[currentCoordinates.x][currentCoordinates.y];
            path.add(currentCoordinates);
        }
        Collections.reverse(path);
        return path;
    }

    /***
     * Tworzy tablice dwuwymiarowa odleglosci miedzy glowa weza a zaba
     * @param frog zaba dla ktorej liczymy odleglosc
     * @return tablica dwuwymiarowa odleglosci
     */
    public int[][] createFrogMatrix(Frog frog) {
        ArrayList<Cords> snakeHeads = new ArrayList<>();
        try {
            snakeHeads.add(this.board.getEnemySnake().getSnakeHead());
            snakeHeads.add(this.board.getPlayerSnake().getSnakeHead());
        } catch (Exception e) {
            snakeHeads.add(this.board.getPlayerSnake().getSnakeHead());
        }
        int[][] snakeDistances = getDistancesMatrix(snakeHeads, BoardTile.ENEMY_SNAKE_HEAD);

        ArrayList<Cords> frogList = new ArrayList<>();
        frogList.add(frog.getCoordinates());
        int[][] frogDistances = getDistancesMatrix(frogList, BoardTile.FROG);
        int[][] outputMatrix = new int[this.board.getWidth()][this.board.getHeight()];

        for (int i=0; i < this.board.getWidth(); i++) {
            for (int j=0; j < this.board.getHeight(); j++) {
                outputMatrix[j][i] = snakeDistances[j][i] - frogDistances[j][i];
                if (this.boardTiles[j][i] == BoardTile.FROG || this.boardTiles[j][i] == BoardTile.FRUIT) {
                    outputMatrix[j][i] = 0;
                }
            }

        }
        return outputMatrix;
    }
}
